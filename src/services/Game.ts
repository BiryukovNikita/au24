class Game {
	length: number = 4;
	public secret: string = '';

	constructor() {
		this.generateValue();
	}

	generateValue() {
		// @ts-ignore
		this.secret = ('' + ~~(Math.random() * 1e4)).padStart(this.length, '0');
	}

	guess(attempt: string): { response: string, win: boolean } {
		if (attempt.length !== this.length)
			throw new Error(`Wrong string length. It should be ${this.length} characters long!`);

		const attemptArray = attempt.split('');
		if (attemptArray.find(letter => isNaN(parseInt(letter))))
			throw new Error('Some letters is not a numbers. All characters should be numbers!');

		let win = true;
		const response = attemptArray.map((number, i) => {
			if (this.secret[i] === number) return 'B';
			win = false;
			if (this.secret.includes(number)) return 'K';
			return '*';
		}).join('');

		return { response, win };
	}
}

export const game = new Game();
