import * as assert from 'assert';
import { game } from './Game';

describe('game', () => {
	it('game should starts with generated secret', () => {
		assert(game.secret);
	});

	describe('guess', () => {
		it('wrong length', done => {
			try {
				game.guess('00');
			} catch (e) {
				done(false);
			}
		});

		it('attempt includes letters', done => {
			try {
				game.guess('abcd');
			} catch (e) {
				done(false);
			}
		});

		it('check result - nothing found', () => {
			game.secret = '1234';
			const { response } = game.guess('0000');
			assert(response === '****');
		});

		it('check result - found at other place', () => {
			game.secret = '1234';
			const { response } = game.guess('0001');
			assert(response === '***K');
		});

		it('check result - found exactly', () => {
			game.secret = '1234';
			const { response } = game.guess('0004');
			assert(response === '***B');
		});

		describe('win', () => {
			it('not win', () => {
				game.secret = '1234';
				const { win } = game.guess('0000');
				assert(!win);
			});

			it('win', () => {
				game.secret = '1234';
				const { win } = game.guess('1234');
				assert(win);
			});
		});
	});
});
