import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { GameView } from './view/GameView';

const app = document.createElement('div');
document.body.appendChild(app);
ReactDOM.render(<GameView />, app);