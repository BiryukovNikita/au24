import * as React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { game } from '../services/Game';

import 'bootstrap/dist/css/bootstrap.min.css';

interface IState {
	attempt: string,
	log: {
		attempt: string,
		result?: string,
		error?: string
	}[],
	win: boolean,
	isDescriptionShown: boolean
}

interface IProps {}

export class GameView extends React.Component<IProps, IState> {
	private input = React.createRef<HTMLInputElement>();
	public state = {
		attempt: '',
		log: [],
		win: false,
		isDescriptionShown: false
	};

	componentDidMount() {
		this.focusInput();
	}

	componentDidUpdate() {
		this.focusInput();
	}

	focusInput() {
		this.input.current.focus();
	}

	handleAttempt = ({ target: { value: attempt } }) =>
		this.setState({ attempt });

	handleSubmit = (e) => {
		e.preventDefault();

		const { attempt, log } = this.state;

		try {
			const { response: result, win } = game.guess(attempt);
			this.setState({
				attempt: '',
				log: [...log, { attempt, result }],
				win
			});
		} catch (error) {
			this.setState({
				attempt: '',
				log: [...log, { attempt, error }],
				win: false
			});
		}
	};

	handleRestart = () => {
		this.setState({
			attempt: '',
			log: [],
			win: false
		});
		game.generateValue();
	};

	handleShowDescription = () =>
		this.setState({ isDescriptionShown: !this.state.isDescriptionShown });

	render() {
		const {
			attempt,
			log,
			win,
			isDescriptionShown
		} = this.state;

		const lastAttempt = log[log.length - 1];

		return (
			<Container>
				<Row>
					<Col xs={12} md={{size: 8, offset: 2}} lg={{size: 6, offset: 3}}>
						<form onSubmit={this.handleSubmit}>
							<h3>Guess a <span title={game.secret}>number</span>!</h3>
							<div>
								<input value={attempt} onChange={this.handleAttempt} ref={this.input}/>
								&nbsp;
								<Button color='primary'>Try!</Button>
							</div>
							<Button color='danger' onClick={this.handleRestart}>Restart</Button>
							{ win && (
								<div>
									<h5>You are win!</h5>
									<div>It lasts {log.length} attempts.</div>
								</div>
							)}
							{ !win && lastAttempt && (
								<div>
									<h5>Last attempt:</h5>
									<div>{lastAttempt.result} {lastAttempt.error && `Error: ${lastAttempt.error}`}</div>
								</div>
							)}
							{lastAttempt && (
								<div>
									<h5>Log:</h5>
									<ul>
										{log.map(({ attempt, result, error}, i) => (
											<li key={i}>
												{attempt}	- {error ?	`Error: ${error}` 	:	result}
											</li>
										))}
									</ul>
								</div>
							)}

							<div>
								{isDescriptionShown
									? (
										<>
											<h5><a href='#' onClick={this.handleShowDescription}>Description:</a></h5>
											<ul>
												<li>* - number not found</li>
												<li>К - number found at other place</li>
												<li>В - number found at that place</li>
											</ul>
										</>
									) : (
										<a href='#' onClick={this.handleShowDescription}>Click to show description</a>
									)
								}
							</div>
						</form>
					</Col>
				</Row>
			</Container>
		);
	}
}
