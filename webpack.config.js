const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/index.tsx',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.json'],
	},
	module: {
		rules: [
			{
				// Include ts, tsx, and js files.
				test: /\.(tsx?)|(js)$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
			}, {
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader',
				],
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin(),
	],
	mode: 'development',
	watch: true,
	devtool: 'source-map',
	devServer: {
		host: '0.0.0.0',
	},
};
